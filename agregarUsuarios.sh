#!/bin/bash

#Limpio la pantalla

clear
#Imprimo el cartel en pantalla

tput cup 1 18 ; echo -n "************************"
tput cup 2 18 ; echo -n "SCRIPT PARA CREAR USUARIOS"
tput cup 3 18 ; echo -n "************************"
#Me fijo si mi id es 0 (correspondiente a root) de lo contrario salgo
#ya que necesito permisos de root para crear usuarios nuevos

if [ "id -u" != 0 ] ; then
echo -e "\n"
echo "Necesito tener permisos de root para ejecutar mis tareas,"
exit 1
fi

if [ "$UID" -ne "$ROOT_UID" ]
then
  echo "Se debe estar como root para ejecutar este script"
  exit $E_NOTROOT
fi  

echo -e "\n"
echo -e "\n"
echo "Ingrese el nombre del nuevo usuario: "
read nombre
#Si el nombre del usuario está vacío entonces salgo del script

if [ "$nombre" == "" ] ; then
clear
echo -e "\n"
echo "El nombre no puede estar vacío"
echo -e "\n"
exit 1
fi

echo -e "\n"
echo "Ingrese el home directoy (default /home/$nombre): "
read home
#Si no ingreso ningún directorio home dejo el que viene por defecto

if [ "$home" == "" ] ; then
home="/home/$nombre"
fi
#Ingreso el grupo 1000 que generalmente es el de usuarios comunes por defecto

echo "Ingrese el grupo (default 1000): "
read grupo

if [ "$grupo" == "" ] ; then
grupo=1000
fi

echo "Ingrese el shell a utilizar (default /bin/bash): "
read shell
#Si no ingreso ningún shell como por ejemplo un menu, dejo con el shell por defecto

if [ "$shell" == "" ] ; then
shell="/bin/bash"
fi
#Limpio la pantalla

clear
#Hago un resumen de los datos ingresados

echo "Los datos ingresados son los siguientes:"
echo "****************************************"
echo -e "\n"
echo "Nombre del usuario: $nombre"
echo "Shell por defecto : $shell"
echo "Directorio home: $home"
echo "Grupo del usuario: $grupo"
echo "-------------------------------------------"
echo "Si los datos son correctos pulse una tecla,"
echo "de lo contrario pulse Ctrl+C para cancelar"
read
#Creo el usuario con las variables

useradd -d /home/$nombre -g $grupo -m -s $shell $nombre

#Si hay un error salgo del script

if [ "$?" != 0 ] ; then
echo -e "\n"
echo "Ha ocurrido un error, asegurese de que los datos ingresados son correctos."
exit 1
fi
#Por último pido la contraseña

echo -e "\n"
echo "Ingrese una contraseña para $nombre"
passwd $nombre
#Si la contraseña no dio ningún error y llegamos a este paso el usuario ya esta creado :)

echo -e "\n"
echo "El usuario fue creado satisfactoriamente"

#Archivo Perl con el que la encripta

#!/usr/local/bin/perl
#
call it like so: perl crypt.pl password

srand (time());
my $randletter = "(int (rand (26)) + (int (rand (1) + .5) % 2 ? 65 : 97))";
my $salt = sprintf ("%c%c", eval $randletter, eval $randletter);
my $plaintext = shift;
my $crypttext = crypt ($plaintext, $salt);

print "${crypttext}";